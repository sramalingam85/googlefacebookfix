const express = require('express');
const router = express.Router();

// Referral Model
const Referral = require('../../models/Referral');

// @route GET api/referral
// @desc Get All referral
// @access Public
router.get('/', (req, res) => {
    const referral = Referral.find()
      .sort({createddatetime : -1})
    res.send(referral)
});

// @route POST api/referral
// @desc Create a referral
// @access Public
router.post('/', async(req, res) => {
    const newReferral = new Referral({
        userid: req.body.userid,
        email: req.body.email,
        referrals: req.body.referrals
    });
    console.log(newReferral)
    await newReferral.save(function(err){
      if (err)
        res.send(err);
      res.json(newReferral);
    });
});

// @route DELETE api/referral/:id
// @desc Delete a referral
// @access Public
router.delete('/:id', (req, res) => {
    Referral.findById(req.params.id)
      .then(referral => referral.remove()
        .then(() => res.json({ success: true })))
    .catch(err => res.status(404).json({success : false}));
});

module.exports = router;