const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

//MongoDB config
const keys = require("../../config/keys");

// register load input data 
const validateRegisterInput = require("../../validation/register");

// login input data 
const validateLoginInput = require("../../validation/login");

// user schema data
const Customer = require("../../models/Customer");


// @route POST api/users/register
// @desc Register user
// @access Public
// register form vaildation 

router.post("/register", (req, res) => {
  // Form validation
const { errors, isValid } = validateRegisterInput(req.body);

// Check validation form
 
  if (!isValid) {
    return res.status(400).json(errors);
  }
Customer.findOne({ email: req.body.email }).then(customer => {
    if (customer) {
      return res.status(400).json({ email: "Email already exists" });
    } else {
      const newCustomer = new Customer({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password
      });

// Hash password before saving in database like encryte and decryted

      bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(newCustomer.password, salt, (err, hash) => {
          if (err) throw err;
          newCustomer.password = hash;
          newCustomer
            .save()
            .then(user => res.json(customer))
            .catch(err => console.log(err));
        });
      });
    }
  });
});


// @route POST api/users/login

// @desc Login user and return JWT token

// @access Public

router.post("/login", (req, res) => {
  // Form validation
const { errors, isValid } = validateLoginInput(req.body);
// Check validation
  if (!isValid) {
    return res.status(400).json(errors);
  }
const email = req.body.email;
  const password = req.body.password;
// Find user by email
  Customer.findOne({ email }).then(customer => {
    // Check if user exists
    if (!customer) {
      return res.status(404).json({ emailnotfound: "Email not found" });
    }
// Check password
    bcrypt.compare(password, customer.password).then(isMatch => {
      if (isMatch) {
        // User matched
        // Create JWT Payload
        const payload = {
          id: customer.id,
          name: customer.name
        };
// Sign token
        jwt.sign(
          payload,
          keys.secretOrKey,
          {
            expiresIn: 31556926 // 
          },
          (err, token) => {
            res.json({
              success: true,
              token: "Bearer " + token
            });
          }
        );
      } else {
        return res
          .status(400)
          .json({ passwordincorrect: "Password incorrect" });
      }
    });
  });
});

module.exports = router;