const express = require("express");
const mongoose = require("mongoose");
const passport = require("passport");


// Location of the customer schema needs exported
// this schema contains the customer information
const customers = require("./routes/api/customers");
const referral = require('./routes/api/referral');


const app = express();
const bodyParser = require('body-parser');

// Use express parser - replaced body Parser.json
app.use(express.json());


// MonogoDb  Configuration
const db = require("./config/Keys").mongoURI;


// Connect to MongoDB
mongoose
  .connect(
    db,
    { useNewUrlParser: true,useUnifiedTopology: true  }
  )
  .then(() => console.log("MongoDB successfully connected"))
  .catch(err => console.log(err));


// Passport middleware for authencation
app.use(passport.initialize());

// Passport config to maintaince the authencation datas
require("./config/passport")(passport);


// Use routes
// Define the application to use customer/referral js file for customer api calls
app.use('/api/customers', customers);
app.use('/api/referral', referral);
// app.use('/api/users',users);
 

 // Create port and listen
const port = process.env.PORT || 5000; 
app.listen(port, () => console.log(`Server up and running on port ${port} !`));
