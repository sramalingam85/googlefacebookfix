import React, { Component } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { loginCustomer } from "../../redux/actions/authActions";
import classnames from "classnames";
import { loginWithGoogle } from "../../actions/authActions";
import { loginWithFacebook } from "../../actions/authActions";
import { GoogleLogin } from 'react-google-login';
import FacebookLogin from 'react-facebook-login';

class Login extends Component {
  constructor() {
    super();
    this.state = {
      email: "",
      password: "",
      isGoogleLoggedIn: false,
      googleUserId: '',
      readOnly: true,
      isFacebookLoggedIn: false,
      facebookUserId: '',
      errors: {}
    };
  }

  componentDidMount() {
    if (this.props.auth.isAuthenticated) {
      this.props.history.push("/dashboard");
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.auth.isAuthenticated) {
      this.props.history.push("/dashboard");
    }

    if (nextProps.errors) {
      this.setState({
        errors: nextProps.errors
      });
    }
  }

  onChange = e => {
    this.setState({ [e.target.id]: e.target.value });
  };

  onSubmit = e => {
    e.preventDefault();

    const customerData = {
      email: this.state.email,
      password: this.state.password
    };

    this.props.loginCustomer(customerData);
    / When facebook button is clicked.
  componentClicked = () => {
    console.log( 'facebook login btn clicked' );
  };

  /**
   * LOGIN VIA FACEBOOK
   * Once you get the response call the function to login the user.
   */
  responseFacebook = ( response ) => {
    // Check if the user has verified the Recaptcha


      // You will get response if user successfully logs in using Facebook.
      this.setState({
        isFacebookLoggedIn: true,
        isGoogleLoggedIn: false,
        facebookUserId: response.id,
        userNameOrEmail: response.email,
        picture: response.picture.data.url,
      });
      const customerData = {
        email: this.state.name,
        facebookUserId: this.state.facebookUserId,
        errors: this.state.errors
      };

      this.props.loginWithFacebook( customerData, this.props.history );
    }
  responseGoogle = (response) => {


      // You will get response if user successfully logs in using Facebook.
      this.setState({
        isGoogleLoggedIn: true,
        isFacebookLoggedIn: false,
        googleUserId: response.googleId,
        userNameOrEmail: response.w3.U3,
        picture: response.profileObj.imageUrl,
      });
      const userData = {
        email: this.state.userNameOrEmail,
        googleUserId: this.state.googleUserId,
        errors: this.state.errors
      };

      this.props.loginWithGoogle( userData, this.props.history );
    } 
  

  };

  render() {
    const { errors } = this.state;
    let fbContent;
    // If the user is logged via facebook show his picture in place of button else show facebook login button.
    if ( this.state.isFacebookLoggedIn ) {
      fbContent = (
        <div>
          <img src={ this.state.picture } alt={ this.state.name } />
          <h6>Welcome { this.state.name }</h6>
        </div>
      );
    } else {
      fbContent =   <FacebookLogin
        appId='1260345724312668'
        autoLoad={ false }
        fields="name,email,picture"
        size="small"
        textButton="Login with Facebook"
        onClick={ this.componentClicked }
        callback={ this.responseFacebook } 
        />
    }
    // FOR GOOGLE
    let googleContent;
    // If the user is logged via facebook show his picture in place of button else show facebook login button.
    if ( this.state.isGoogleLoggedIn ) {
      googleContent = (
        <div>
          <img src={ this.state.picture } alt={ this.state.customerName } />
          <h6>Welcome { this.state.customerName }</h6>
        </div>
      );
    } else {
      googleContent =     <GoogleLogin
        clientId='389518499066-0m06rndknv9d18kfo7j6cr4qtvd5kh0j.apps.googleusercontent.com'
        buttonText="LOGIN WITH GOOGLE"
        style={{fontSize: '14px', padding: '12px 24px 12px 24px', background: 'rgb(209, 72, 54)', color: '#fff', cursor: 'pointer' }}
        onSuccess={this.responseGoogle}
        onFailure={this.responseGoogle}
      />
    }

    return (

      <>

      <section className="login-form">
        <div  className="form-l-title">
          <h3>Log in with your email</h3>

            <form noValidate onSubmit={this.onSubmit}>
              <div className="form-l-text">
                  <label>Email</label>
                <input
                  onChange={this.onChange}
                  value={this.state.email}
                  error={errors.email}
                  id="email"
                  type="email"
                  className={classnames("", {
                    invalid: errors.email || errors.emailnotfound
                  })}
                />
              
                <span>
                  {errors.email}
                  {errors.emailnotfound}
                </span>
              </div>
              <div className="form-l-text">
               <label>Password</label>
                <input
                  onChange={this.onChange}
                  value={this.state.password}
                  error={errors.password}
                  id="password"
                  type="password"
                  className={classnames("", {
                    invalid: errors.password || errors.passwordincorrect
                  })}
                />
                <span>
                  {errors.password}
                  {errors.passwordincorrect}
                </span>
              </div>
              <div className="form-l-button">
                <button
                  type="submit">
                  Login
                </button>
              </div>
            </form>
              <div className="form-l-bottom">
              <p>
                Don't have an account? <Link to="/register">Register</Link>
              </p>
              </div>
              <div>
               {googleContent} 
              </div> 
              <div>
               {fbContent} 
              </div>                
        </div>
      </section>
    </>

    );
  }
}





Login.propTypes = {
  loginCustomer: PropTypes.func.isRequired,
  loginWithGoogle: PropTypes.func.isRequired,
  loginWithFacebook: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default connect(
  mapStateToProps,
  { loginCustomer,loginWithGoogle,loginWithFacebook }
)(Login);