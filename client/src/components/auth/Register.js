
import React, { Component } from "react";
import { Link,withRouter } from "react-router-dom";
import logintrans from "../images/logintrans.png"
import './Login.css';
import classnames from "classnames";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { registerCustomer } from "../../redux/actions/authActions";


class Register extends Component {
  constructor() {
    super();
    this.state = {
      email: "",
      name:"",
      password: "",
      errors: {}
    };
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({
        errors: nextProps.errors
      });
    }
  }
  componentDidMount() {
    // If logged in and user navigates to Register page, should redirect them to dashboard
    if (this.props.auth.isAuthenticated) {
      this.props.history.push("/profile");
    }
  }
  onChange = e => {
    this.setState({ [e.target.id]: e.target.value });
  };

onSubmit = e => {
    e.preventDefault();

const newCustomer = {
      name: this.state.name,
      email: this.state.email,
      password: this.state.password
    };

this.props.registerCustomer(newCustomer, this.props.history); 
};

  render() {

    const { errors } = this.state;
    return (

      <>

      <section className="login-form">
        <div  className="form-l-title">
          <div className="logo-ref">
           <img src={logintrans} alt="reflogo" />
           <p>Sign up to refer and make money!</p>
           </div>
             <form noValidate onSubmit={this.onSubmit}>
              <div className="form-l-text">
                 <input
                  onChange={this.onChange}
                  value={this.state.email}
                   error={errors.email}
                  type="email"
                  id="email"
                  placeholder="Email or Phone"
                  className={classnames("", {
                    invalid: errors.email
                  })}
                />
                 <span className="red-text">{errors.email}</span>
              </div>
               <div className="form-l-text">
                 <input
                  onChange={this.onChange}
                  value={this.state.name}
                   error={errors.name}
                  type="name"
                  id="name"
                  placeholder="Name"
                   className={classnames("", {
                    invalid: errors.name
                  })}
                />
                 <span className="red-text">{errors.name}</span>
              </div>
              <div className="form-l-text">
                   <input
                  onChange={this.onChange}
                  value={this.state.password}
                   error={errors.password}
                  type="password"
                  id="password"
                  placeholder="Password"
                  className={classnames("", {
                    invalid: errors.password
                  })}
                />
                 <span className="red-text">{errors.password}</span>
              </div>
              <div className="form-l-button">
                <button  type="submit">
                  Signup
                </button>
              </div>
            </form>
              <div className="form-l-bottom">
              <p>By signing up, you agree to Terms and Conditions, Cookie Policy</p>
              <p>
                Already have an account? <Link to="/login">Login</Link>
              </p>
              </div>

        </div>
      </section>
    </>
    );
  }
}
Register.propTypes = {
  registerCustomer: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};
const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});
export default connect(
  mapStateToProps,
  { registerCustomer}
)(withRouter(Register));


