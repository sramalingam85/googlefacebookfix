import React, { Component } from "react";
import { Navbar,Nav,NavItem} from 'react-bootstrap';
import {Link } from "react-router-dom";
import { logoutCustomer } from "../redux/actions/authActions";  
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

class Headerbar extends Component {

    onLogout(e) {
        e.preventDefault();
        this.props.logoutCustomer(this.props.history);
    }

    render() {
        const {isAuthenticated} = this.props.auth;
        const authLinks = (
            <Nav className="ml-auto">
                <Link className="nav-link" onClick={this.onLogout.bind(this)}>
                            Logout
                </Link>
            </Nav>
        )
      const guestLinks = (
        <Nav className="ml-auto">
            <NavItem className="user">
                <Link  to='/homemain'>Home</Link>
                <Link className="userregister" to="/register">Sign Up</Link>
                <Link className="userlogin" to="/login">Sign In</Link>
             </NavItem>
        </Nav>
      )
        return(
        <>
             <Navbar bg="dark" variant="dark" fixed="top">
                 <Navbar.Brand as={Link} to="/">Demo App</Navbar.Brand>
                    {isAuthenticated ? authLinks : guestLinks}
            </Navbar>
        </>
        )
    }
}

Navbar.propTypes = {
    logoutCustomer: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
    auth: state.auth
})

export default connect(mapStateToProps, { logoutCustomer })(withRouter(Headerbar));





