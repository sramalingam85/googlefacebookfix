import React from 'react';
import './Footer.css';

function Footer() {
  return (
    <div className='footer-section'>
      <div className="footer-all">
        <div class='footer-items'>
          <h2>Main</h2>
          <p>Blog</p>
          <p>FAQs</p>
          <p>Support</p>
          <p>About US</p>
        </div>
        <div class='footer-items'>
          <h2>Main</h2>
          <p>Blog</p>
          <p>FAQs</p>
          <p>Support</p>
          <p>About US</p>
        </div>
        <div class='footer-items'>
          <h2>Main</h2>
          <p>Blog</p>
          <p>FAQs</p>
          <p>Support</p>
          <p>About US</p>
        </div>
        <div class='footer-items'>
          <h2>Main</h2>
          <p>Blog</p>
          <p>FAQs</p>
          <p>Support</p>
          <p>About US</p>
        </div>
        <div class='footer-items'>
          <h2>Main</h2>
          <p>Blog</p>
          <p>FAQs</p>
          <p>Support</p>
          <p>About US</p>
        </div>
      </div>
      <div className="footer-bottom">
        <p>© PlugMesoft. All Rights Reserved.</p>
      </div>
    </div>
  );
}

export default Footer;
