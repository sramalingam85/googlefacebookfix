import React from 'react';
import HeroSlider from './HeroSlider';
import Service from './Service';
import Pricing from './Pricing';
import Testimony from './Testimony';
import Footer from './Footer';

function HomeMain() {
  return (
    <>
      <HeroSlider />
      <Service />
      <Pricing />
      <Testimony />
      <Footer />
    </>
  );
}

export default HomeMain;