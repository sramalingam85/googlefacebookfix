import React from 'react';
import testimony from "../images/testimony.svg";
import Slider from "react-slick";
import './Testimony.css';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";


class Testimony extends React.Component {
	render() {

		var settings = {
			dots: false,
			infinite: true,
			speed: 500,
			slidesToShow: 1,
			slidesToScroll: 1
		};

		return (
			<>
				<section className="testimony-main" id="testimonial">
					<div className="testimony-image">
						<img src={testimony} alt="care" />
					</div>
					<div className="testimony-review">
						<h5>TESTIMONIALS</h5>
						<h2>Our Clients <span>Love Us.</span></h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
    				 incididunt ut labore et dolore magna aliqua enim ad minim veniam.</p>
						<Slider {...settings}>
							<div>
								<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
    				 incididunt ut labore et dolore magna aliqua enim ad minim veniam</h3>
							</div>
							<div>
								<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
    				 incididunt ut labore et dolore magna aliqua enim ad minim veniam</h3>
							</div>
						</Slider>
					</div>
				</section>
			</>
		)
	}
}


export default Testimony;