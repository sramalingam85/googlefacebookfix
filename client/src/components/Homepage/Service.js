import React from 'react'
import './Service.css';
import SupportIconImage from "../images/support-icon.svg";
import ShieldIconImage from "../images/shield-icon.svg";
import CustomizeIconImage from "../images/customize-icon.svg";
import FastIconImage from "../images/fast-icon.svg";
import ReliableIconImage from "../images/reliable-icon.svg";
import SimpleIconImage from "../images/simple-icon.svg";


export default function Service() {
	return (
		<>
			<section className="service-main" id="services">
				<div className="service-heading">
					<h5>FEATURES</h5>
					<h2>We have Amazing <span>Service.</span></h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
            		sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
				</div>
				<div className="service-content">
					<div className="service-card">
						<div className="service-inner">
							<div className="service-card-image">
								<img src={ShieldIconImage} alt="secure" />
							</div>
							<div className="service-card-content">
								<h2>Secure</h2>
								<p>We strictly only deal with vendors that provide top notch security.</p>
							</div>
						</div>
					</div>
					<div className="service-card">
						<div className="service-inner">
							<div className="service-card-image">
								<img src={SupportIconImage} alt="secure" />
							</div>
							<div className="service-card-content">
								<h2>24/7 Support</h2>
								<p>	Lorem ipsum donor amet siti ceali ut enim ad minim veniam, quis nostrud.</p>
							</div>
						</div>
					</div>
					<div className="service-card">
						<div className="service-inner">
							<div className="service-card-image">
								<img src={CustomizeIconImage} alt="secure" />
							</div>
							<div className="service-card-content">
								<h2>Customizable</h2>
								<p>Lorem ipsum donor amet siti ceali ut enim ad minim veniam, quis nostrud.</p>
							</div>
						</div>
					</div>
					<div className="service-card">
						<div className="service-inner">
							<div className="service-card-image">
								<img src={ReliableIconImage} alt="secure" />
							</div>
							<div className="service-card-content">
								<h2>Reliable</h2>
								<p>	Lorem ipsum donor amet siti ceali ut enim ad minim veniam, quis nostrud.</p>
							</div>
						</div>
					</div>
					<div className="service-card">
						<div className="service-inner">
							<div className="service-card-image">
								<img src={FastIconImage} alt="secure" />
							</div>
							<div className="service-card-content">
								<h2>Fast</h2>
								<p>	Lorem ipsum donor amet siti ceali ut enim ad minim veniam, quis nostrud.</p>
							</div>
						</div>
					</div>
					<div className="service-card">
						<div className="service-inner">
							<div className="service-card-image">
								<img src={SimpleIconImage} alt="secure" />
							</div>
							<div className="service-card-content">
								<h2>Easy</h2>
								<p>Lorem ipsum donor amet siti ceali ut enim ad minim veniam, quis nostrud.</p>
							</div>
						</div>
					</div>
				</div>
			</section>
		</>
	)

}





