import React from "react";
import DesignIllustration from "../images/design-illustration-2.svg";
import './HeroSlider.css';


function HeroSlider() {
  return (
    <>
      <section className="heropage-main">
        <div className="heropage-content">
          <h1>
            Beautiful React Templates <span>for you.</span>
          </h1>
          <p>
            Our templates are easy to setup, understand and customize. Fully modular components with a variety of
            pages and components.
              </p>
        </div>
        <div className="heropage-image">
          <img src={DesignIllustration} alt="Design Illustration" />
        </div>
      </section>
    </>
  );
}

export default HeroSlider;
