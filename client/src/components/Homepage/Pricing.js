import React from 'react'
import './Pricing.css';

export default function Pricing() {
    return (

        <>
            <section className="Pricing-main" id="pricing">
                <div className="Pricing-heading">
                    <h5>PRICING</h5>
                    <h2>Reasonable & Flexible  <span>Plans.</span></h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                </div>
                <div className="pricing-card-all">
                    <div className="pricing-card">
                        <div className="design"></div>
                        <div className="card-head">
                            <p className="name"> PERSONAL</p>
                            <p className="price">$17.99</p>
                            <p className="duration">MONTHLY</p>
                        </div>
                        <div className="card-body">
                            <p>For Individuals</p>
                            <p>30 Templates</p>
                            <p> 7 Landing Pages</p>
                            <p>12 Internal Pages</p>
                            <p>Basic Assistance</p>
                        </div>
                        <div className="card-footer">
                            <button>BUY NOW</button>
                        </div>
                    </div>
                    <div className="pricing-card">
                        <div className="design"></div>
                        <div className="card-head">
                            <p> PERSONAL</p>
                            <p>$17.99</p>
                            <p>MONTHLY</p>
                        </div>
                        <div className="card-body">
                            <p>For Individuals</p>
                            <p>30 Templates</p>
                            <p> 7 Landing Pages</p>
                            <p>12 Internal Pages</p>
                            <p>Basic Assistance</p>
                        </div>
                        <div className="card-footer">
                            <button>BUY NOW</button>
                        </div>
                    </div>
                    <div className="pricing-card">
                        <div className="design"></div>
                        <div className="card-head">
                            <p> PERSONAL</p>
                            <p>$17.99</p>
                            <p>MONTHLY</p>
                        </div>
                        <div className="card-body">
                            <p>For Individuals</p>
                            <p>30 Templates</p>
                            <p> 7 Landing Pages</p>
                            <p>12 Internal Pages</p>
                            <p>Basic Assistance</p>
                        </div>
                        <div className="card-footer">
                            <button>BUY NOW</button>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )

}