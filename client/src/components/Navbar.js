import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { FaBars} from 'react-icons/fa';
import { logoutCustomer } from "../redux/actions/authActions";  
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import './Navbar.css'



class Navbar extends Component {
    state = { clicked: false }

    handleClick = () => {
        this.setState({ clicked: !this.state.clicked })
    }

     onLogout(e) {
        e.preventDefault();
        this.props.logoutCustomer(this.props.history);
    }

    render() {
             const {isAuthenticated} = this.props.auth;
        const authLinks = (
            <nav className="ml-auto">
                <Link className="nav-link" onClick={this.onLogout.bind(this)}>
                            Logout
                </Link>
            </nav>
        )
      const guestLinks = (
        <nav className="ml-auto">
            <navitem className="user">
                <Link className="nav-links" to='/'>Home</Link>
                <Link className="nav-links" to="/register">Sign Up</Link>
                <Link className="nav-links" to="/login">Sign In</Link>
             </navitem>
        </nav>
      )
        return (
            <nav className="NavbarItems">
                <h1 className="navbar-logo">React</h1>
                <div className="menu-icon" onClick={this.handleClick}>
                    <FaBars className={this.state.clicked}/>
                </div>
                <ul className={this.state.clicked ? 'nav-menu active' : 'nav-menu'}>
                   {isAuthenticated ? authLinks : guestLinks}
                </ul>
            </nav>     
        )
    }
}

Navbar.propTypes = {
    logoutCustomer: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
    auth: state.auth
})

export default connect(mapStateToProps, { logoutCustomer })(withRouter(Navbar));

