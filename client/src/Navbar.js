import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { FaBars} from 'react-icons/fa';
import { Navbar,Nav,NavItem} from 'react-bootstrap';
import {Link } from "react-router-dom";
import { logoutCustomer } from "../redux/actions/authActions";  
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import './Navbar.css'



class Navbar extends Component {
    state = { clicked: false }

    handleClick = () => {
        this.setState({ clicked: !this.state.clicked })
    }

     onLogout(e) {
        e.preventDefault();
        this.props.logoutCustomer(this.props.history);
    }

    render() {
             const {isAuthenticated} = this.props.auth;
        const authLinks = (
            <Nav className="ml-auto">
                <Link className="nav-link" onClick={this.onLogout.bind(this)}>
                            Logout
                </Link>
            </Nav>
        )
      const guestLinks = (
        <Nav className="ml-auto">
            <NavItem className="user">
                <Link  to='/homemain'>Home</Link>
                <Link className="nav-links" to="/register">Sign Up</Link>
                <Link className="nav-links" to="/login">Sign In</Link>
             </NavItem>
        </Nav>
      )
        return (
            <nav className="NavbarItems">
                <h1 className="navbar-logo">React</h1>
                <div className="menu-icon" onClick={this.handleClick}>
                    <FaBars className={this.state.clicked}/>
                </div>
                <ul className={this.state.clicked ? 'nav-menu active' : 'nav-menu'}>
                   {isAuthenticated ? authLinks : guestLinks}
                </ul>
            </nav>     
        )
    }
}

Navbar.propTypes = {
    logoutCustomer: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
    auth: state.auth
})

export default connect(mapStateToProps, { logoutCustomer })(withRouter(Navbar));

