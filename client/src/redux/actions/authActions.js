import axios from "axios";
import setAuthToken from "../utils/setAuthToken";
import jwt_decode from "jwt-decode";
import {
  GET_ERRORS,
  SET_CURRENT_CUSTOMER,
  CUSTOMER_LOADING
} from "./types";



// Register User authecations
export const registerCustomer = (customerData, history) => dispatch => {
  axios
    .post("/api/customers/register",customerData)
    .then(res => history.push("/login")) 
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Login - get user token
export const loginCustomer = customerData => dispatch => {
  axios
    .post("/api/customers/login", customerData)
    .then(res => {
      const { token } = res.data;
      localStorage.setItem("jwtToken", token);
      setAuthToken(token);
      const decoded = jwt_decode(token);
      dispatch(setCurrentCustomer(decoded));
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Login with Google- Get User Token.
export const loginWithGoogle = ( customerData, history ) => dispatch => {

  axios.post( '/api/customers/loginViaGoogle', customerData )
    .then( ( result ) => {
     
      const { token } = result.data;

      // Store token in localStorage
      localStorage.setItem( 'jwtToken', token );

      // Set token to Auth Header using a custom function setAuthToken
      setAuthToken( token );

      // Use jwt-decode to decode the auth token and get the user data from it( install jwt-decode in clients dir )
      const decoded = jwt_decode( token );

      // Set current user
      dispatch( setCurrentCustomer( decoded ) );

      // Once he is logged in send him to dashboard
      history.push( '/dashboard' );
    } )
    .catch( ( err ) => dispatch( {
      type: GET_ERRORS,
      payload: err.response.data
    } ) );
};

// Login with Facebook- Get User Token.
export const loginWithFacebook = ( customerData, history ) => dispatch => {

  axios.post( '/api/customers/loginViaFacebook', customerData )
    .then( ( result ) => {
     
      const { token } = result.data;

      // Store token in localStorage
      localStorage.setItem( 'jwtToken', token );

      // Set token to Auth Header using a custom function setAuthToken
      setAuthToken( token );

      // Use jwt-decode to decode the auth token and get the user data from it( install jwt-decode in clients dir )
      const decoded = jwt_decode( token );


      // Set current user
      dispatch( setCurrentCustomer( decoded ) );

      // Once he is logged in send him to dashboard
      history.push( '/dashboard' );
    } )
    .catch( ( err ) => dispatch( {
      type: GET_ERRORS,
      payload: err.response.data
    } ) );
};

export const setCurrentCustomer = decoded => {
  return {
    type: SET_CURRENT_CUSTOMER,
    payload: decoded
  };
};
// User loading
export const setCustomerLoading = () => {
  return {
    type: CUSTOMER_LOADING
  };
};
// Log user out section
export const logoutCustomer = () => dispatch => {
  localStorage.removeItem("jwtToken");
  setAuthToken(false);
  dispatch(setCurrentCustomer({}));
};