import { SET_CURRENT_CUSTOMER, CUSTOMER_LOADING,GET_CUSTOMER_DATA } from "../actions/types";

const isEmpty = require("is-empty");

//user authencations 

const initialState = {
  isAuthenticated: false,
  customer: {},
  loading: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SET_CURRENT_CUSTOMER:
      return {
        ...state,
        isAuthenticated: !isEmpty(action.payload),
        user: action.payload
      };
    case CUSTOMER_LOADING:
      return {
        ...state,
        loading: true
      };
      case GET_CUSTOMER_DATA:
        return{
          ...state,
          customerData:action.payload
      };
    default:
      return state;
  }
}
