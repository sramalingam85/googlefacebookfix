import React, { Component } from "react";
import { BrowserRouter as Router,Switch,Route } from "react-router-dom";
import jwt_decode from "jwt-decode";
import setAuthToken from "./redux/utils/setAuthToken";
import { setCurrentCustomer, logoutCustomer } from "./redux/actions/authActions";
import { Provider } from "react-redux";
import store from "./redux/store";  
import Navbar from "./components/Navbar";
import PrivateRoute from "./components/private-route/PrivateRoute";
import Dashboard from "./components/profile/Dashboard";
import Register from "./components/auth/Register";
import Login from "./components/auth/Login";
import HomeMain from './components/Homepage/HomeMain';


// Check for token to keep user logged in
if (localStorage.jwtToken) {
  // Set auth token header auth
  const token = localStorage.jwtToken;
  setAuthToken(token);
  // Decode token and get user info 
  const decoded = jwt_decode(token);
  // Set user and isAuthenticated
  store.dispatch(setCurrentCustomer(decoded));
  // Check for expired token
  const currentTime = Date.now() / 1000; // to get in milliseconds expierd
  if (decoded.exp < currentTime) {
    // Logout user
    store.dispatch(logoutCustomer());
    // browser history window
    window.location.href = "./login";
  }
}
class App extends Component {
  render() {
    return (
      //state provider from redux
      <Provider store={store}>
  
          <div>
           <Router>
            <Navbar/>
            <Switch>
            <PrivateRoute exact path="/dashboard" component={Dashboard} />
            <Route path="/login" component={Login} />  
            <Route path="/register" component={Register} />
            <Route path="/" component={HomeMain}/>
            </Switch>
            </Router> 
          </div>
        
      </Provider>
    );
  }
}
export default App;