const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Customer Schema
const CustomerSchema = new Schema({
    name: {
        type: String,
    },
    email: {
        type: String,
        required: true,
        lowercase: true
    },
    userid: {
        type: String,
    },
    password: {
        type: String,
        required: true
    },
    firstname: {
        type: String,
        required: false,
    },
    lastname: {
        type: String,
    },
    phone: {
        type: String,
        required: false
    },
    address : {
        line1: String,
        line2: String,
        zipcode: String
    },
    country: {
        type: String,
        default: "US"
    },
    createddatetime: {
        type: Date,
    },
    lastupdatedtime: {
        type: Date,
        default: Date.now
    },
    lastlogin: {
        type: Date,
    },
    customerlevel: {
        type: String
    },
});

// Export Customer Schema
module.exports = Customer = mongoose.model('customer', CustomerSchema)