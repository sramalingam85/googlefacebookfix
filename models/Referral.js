const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Referral Schema
const ReferralSchema = new Schema({
    userid: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    referrals: [{
        itemnumber: Number,
        url: String,
        imageurl: String,
        benefit: Number,
        benefittype: String,
        description: String,
        dateadded: Date,
        lastupdated: Date,
    }],
    createddatetime: {
        type: Date,

    },
    lastupdatedtime: {
        type: Date,
        default: Date.now
    },
    customerlevel: {
        type: String
    },
});

// Export Referral Schema
module.exports = Referral = mongoose.model('referral', ReferralSchema)